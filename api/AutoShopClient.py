import requests
import json
import logging

logging.basicConfig(level=logging.INFO)


class AutoShopHTTPClient:

    def __init__(self, url: str = 'http://127.0.0.1:5000/'):
        self.url = url

    def send_request(self, endpoint, headers=None, data=None):
        response = requests.post(url=self.url + endpoint, headers=headers, data=data)
        logging.info(f'Запрос отправлен: url = {self.url + endpoint} headers = {headers}, data = {data}')
        logging.info(f'Пришедший ответ: url = {self.url + endpoint}, status_code = {response.status_code}, '
                     f'headers = {response.headers}, data = {response.content}')
        return response

    def create(self):
        endpoint = 'create_auto'
        logging.info(f'Запрос на создание автомобиля')
        return self.send_request(endpoint)

    def set_color(self, car_id, color):
        endpoint = 'set/color'
        headers = {"Content-Type": "application/json", "Car-id": car_id}
        data = json.dumps({"color": color})
        logging.info(f'Запрос на определение цвета авто')
        return self.send_request(endpoint, headers, data)

    def set_mark(self, car_id, mark):
        endpoint = 'set/mark'
        headers = {"Content-Type": "application/json", "Car-id": car_id}
        data = mark
        logging.info(f'Запрос на определение марки авто')
        return self.send_request(endpoint, headers, data)

    def set_drive(self, car_id, drive):
        endpoint = 'set/drive'
        headers = {"Content-Type": "application/json", "Car-id": car_id}
        data = json.dumps({"drive": drive})
        logging.info(f'Запрос на определение привода авто')
        return self.send_request(endpoint, headers, data)

    def set_engine_power(self, car_id, engine_power):
        endpoint = 'set/engine_power'
        headers = {"Content-Type": "application/json", "Car-id": car_id}
        data = json.dumps({"power": engine_power})
        logging.info(f'Запрос на определение мощности авто')
        return self.send_request(endpoint, headers, data)

    def info(self, car_id):
        endpoint = 'info'
        headers = {"Content-Type": "application/json", "Car-id": car_id}
        logging.info(f'Запрос на информацию об авто')
        return self.send_request(endpoint, headers)

    def delete(self, car_id):
        endpoint = 'delete'
        headers = {"Content-Type": "application/json", "Car-id": car_id}
        logging.info(f'Запрос на удаление данных об авто')
        return self.send_request(endpoint, headers)
