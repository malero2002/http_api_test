from api.AutoShopClient import AutoShopHTTPClient

http_client = AutoShopHTTPClient()

response_create = http_client.create()
car_id = http_client.requests_model.headers['Car-id']

response_color = http_client.set_color(car_id, "Blue")

response_mark = http_client.set_mark(car_id, "BMW")

response_drive = http_client.set_drive(car_id, "2WD")

response_engine_power = http_client.set_engine_power(car_id, 150)

response_info = http_client.info(car_id)
