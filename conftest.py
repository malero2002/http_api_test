import pytest
from tests_autoshop.test_set_color import http_client


@pytest.fixture()
def create_auto_fixture():
    response = http_client.create()
    car_id = response.headers['Car-id']
    yield car_id

    http_client.delete(car_id)


@pytest.fixture()
def create_auto_gen():
    list_car_id = []

    def _create(number_auto):
        for i in range(number_auto):
            response = http_client.create()
            list_car_id.append(response.headers['Car-id'])
        return list_car_id

    yield _create

    def _delete():
        for car_id in list_car_id:
            http_client.delete(car_id)

    _delete()
