import pytest

from api.AutoShopClient import AutoShopHTTPClient
from tests_autoshop.Errors.Еггоr_output import *

http_client = AutoShopHTTPClient()


def test_status_code():
    response_status_code = http_client.create()
    assert response_status_code.status_code == 201, Error_status_code


def test_car_id():
    response_car_id = http_client.create()
    assert response_car_id.headers['Car-id'] is not None, Error_car_id


def test_type_car_id():
    response = http_client.create()
    assert type(response.headers['Car-id']) == str, Error_type_car_id


def test_len_car_id():
    response = http_client.create()
    assert len(response.headers['Car-id']) == 15, Error_len_car_id


def test_first_number_car_id():
    response = http_client.create()
    response_car_id = response.headers['Car-id']
    assert response_car_id[0] == '1', Error_first_number_car_id
