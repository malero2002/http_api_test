import allure

from api.AutoShopClient import AutoShopHTTPClient

http_client = AutoShopHTTPClient()


@allure.feature('What')
@allure.story('Yes')
def test_status_code(create_auto_gen):
    with allure.step("Тест на статус код"):
        car_id_1, car_id_2 = create_auto_gen(2)
        response = http_client.set_color(car_id_1, 'Blue')
        assert response.status_code == 200


def test_set_color_type_body():
    with allure.step("Тест на тип тела запроса"):
        response = http_client.create()
        car_id = response.headers['Car-id']
        response = http_client.set_color(car_id, 'Blue')
        assert type(response.request.body) == str


def test_set_color_url():
    with allure.step("Тест на соответствие url"):
        response = http_client.create()
        car_id = response.headers['Car-id']
        response = http_client.set_color(car_id, 'Blue')
        assert response.request.url == 'http://127.0.0.1:5000/set/color'


def test_set_color_change():
    with allure.step("Тест на смену цвета"):
        response = http_client.create()
        car_id = response.headers['Car-id']
        response_blue = http_client.set_color(car_id, 'Blue')
        body_blue = response_blue.request.body
        response_white = http_client.set_color(car_id, 'White')
        body_white = response_white.request.body
        assert body_blue != body_white
